#Resize Icons for PhoneGap Build

### What does this do?
This script takes a single icon image and generates the appropriate icon sizes for Android, BlackBerry, iOS, WebOS and Windows.  
  
Your icon must equal in height and width and be  1024 or more pixels on a side.

You can generate for one single environment or for all of them. The prompt will ask you.

The files will be created under /icon. Move this directory before generating a new set of icons.

### What's new?
The script will now also generate all the the splash screen images required for iOS applications. These images are blank but provide the correct sizes.

###To run the script

`ruby resizeIcons_v2.rb filename.png`

After it completes it will create a directory within called icons and one called splash screen (if you chose splashscreen or generate everything).

###Preparing your environment

To run this script on your machine, it has few requirements.

Requirements:  
  
* Ruby    
* ImageMagick (use HomeBrew to install)  
* RMagick

##### Mac OSx issues

If you have been struggling with Rmagick installing it on Mountain Lion, try this:

Reinstall imagemagick with Homebrew:

$ brew remove imagemagick
$ brew install imagemagick --disable-openmp --build-from-source
and then

$ gem install rmagick -v '2.13.2'

##### Windows issues

I would rather take a sharp stick in the eye than try to install ImageMagick on a Windows machine! If you have a solution please update this document.

Any input is appreciated. This was a tool I built after being ready to deploy to the app store only to be curtailed by the plethora of icons required by Apple. Plus, I do quite a bit of cross mobile work so this let's me be "cross mobile" with my icons also.
