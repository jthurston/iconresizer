#Version 2.5 added splashscreen creation

require 'RMagick'
require 'FileUtils'
include Magick

#Give user feedback that they need to provide input.
unless ARGV[0]
  puts "\nYou need to specify a filename:  resizePhoto.rb <filename>\n"
  puts "- Must be a .png file\n"
  puts "- Must be at least 1024 x 1024 in size\n"
  puts "- Must be the same width and height\n\n"
  exit
end

#Building the directory name by getting the file name and removing the .png
#from the end. It is then assigned to global variable.
$dirname = ARGV[0].downcase.chop.chop.chop.chop

#Main routine for making the icons
def makeicons(size, file_name, dir)

  img = Image.read(ARGV[0]).first

  #make sure height matches the width
  def checksize(w, h)
    if w != h
      puts "Image must be the same height and width"
      exit
    end
    #make sure 1024 or larger
    if w < 1024
      puts "Initial image must be 1024 x 1024"
      exit
    end
  end

  checksize(img.columns, img.rows)

  puts "You selected " + dir + " and " + file_name + " will be located in /icon/" + dir
  width = nil
  height = nil

  img.change_geometry!(size) do |cols, rows, img|
    img.resize!(cols, rows)
    width = cols
    height = rows
  end

  if File.exists?("icon-" + $dirname + "/" + dir + "/" + file_name)
    puts file_name + " already exists.  Unable to write file and exiting script."
    exit
  end

  img.write("icon-" + $dirname + "/" + dir + "/" + file_name)
end

prompt = ': '

puts "What type of icons (A)ndroid, (B)lackberry, (I)OS, (W)indows, Web(O)s, (S)plashScreens, (E)very?"
print prompt
type = STDIN.gets.chomp()

# @return [Object]
def testing()
  FileUtils.mkdir_p 'icon-'+$dirname+'test'
  makeicons('36x36', 'icon-36-ldp.png', 'test')
end

def android()

  FileUtils.mkdir_p 'icon-' + $dirname + '/android'
  makeicons('144x144', 'icon-xxhdpi.png', 'android')
  makeicons('96x96', 'icon-xhdpi.png', 'android')
  makeicons('72x72', 'icon-hdpi.png', 'android')
  makeicons('48x48', 'icon-mdpi.png', 'android')
  makeicons('36x36', 'icon-ldpi.png', 'android')
end

def blackberry()
  FileUtils.mkdir_p 'icon-' + $dirname + '/blackberry'
  makeicons('80x80', 'icon-80.png', 'blackberry')
end

def ios()
  FileUtils.mkdir_p 'icon-' + $dirname + '/ios'
  # >= iOS 8.0
  makeicons('180x180', 'icon-60@30x.png', 'ios') #App Icon — iPhone 6 Plus
  makeicons('120x120', 'icon-small-40@3x.png', 'ios') #App Icon — Search — iPhone 6 Plus
  makeicons('87x87', 'icon-small@3x.png', 'ios') #App Icon - Settings — iPhone 6 Plus
  # >= iOS 7.0
  makeicons('60x60', 'icon-60.png', 'ios') #App Icon - iPhone
  makeicons('120x102', 'icon-60@2x.png', 'ios') #App Icon - Retina iPhone
  makeicons('76x76', 'icon-76.png', 'ios') #App Icon — iPad
  makeicons('152x152', 'icon-76@2x.png', 'ios') #App Icon — Retina iPad
  makeicons('40x40', 'icon-small-40.png', 'ios') #Search/Settings — all devices
  makeicons('80x80', 'icon-small-80.png', 'ios') #Search/Settings — all devices
  # >= iOS 6.1
  makeicons('57x57', 'icon.png', 'ios') #iPhone
  makeicons('114x114', 'icon@2x.png', 'ios') #iPhone 4 Retina Display
  makeicons('72x72', 'icon-72.png', 'ios') #iPad
  makeicons('144x144', 'icon-72@2x.png', 'ios') #iPad Retina
  makeicons('50x50', 'icon-small-50.png', 'ios') #Search/Settings iPad
  makeicons('100x100', 'icon-small-50@2x.png', 'ios') #Search/Settings iPad Retina
  makeicons('29x29', 'icon-small.png', 'ios') #Search/Settings iPhone
  makeicons('58x59', 'icon-small@2x.png', 'ios') #Search/Settings iPhone Retina
  # Various icons
  makeicons('320x320', 'icon-320.png', 'ios') #iPad Retina document icon
  makeicons('640x640', 'icon-640.png', 'ios') #iPad Retina icon
  makeicons('512x512', 'iTunesArtwork.png', 'ios') #iTunes and App Store
  makeicons('1024x1024', 'iTunesArtwork@2x.png', 'ios') #iTunes Retina
end

def iossplash()
  FileUtils.mkdir_p 'splashscreen/ios'
#iPhone Portrait iOS 5,6 low
  f = Image.new(320, 480) { self.background_color = "white" }
  f.write("splashscreen/ios/Default.png")
#iPhone Portrait iOS7 high
  f = Image.new(640, 960) { self.background_color = "white" }
  f.write("splashscreen/ios/Default@2x.png")
#iPhone Portrait iOS7 low
  f = Image.new(640, 1136) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-568h@2x.png")

  f = Image.new(750, 1134) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-667@2x.png")

  f = Image.new(1242, 2208) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-736@3x.png")
#Landscape
  f = Image.new(1136, 640) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Landscape-568h@2x.png")

  f = Image.new(1334, 750) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Landscape-667h@2x.png")

  f = Image.new(2208, 1242) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Landscape-736h@3x.png")

#iPad Portrait iOS7 low
  f = Image.new(768, 1024) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Portrait.png")
#iPad Portrait iOS7 high
  f = Image.new(1536, 2048) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Portrait@2x.png")
#iPad Landscape iOS7 low
  f = Image.new(1024, 768) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Landscape.png")
#iPad Landscape iOS7 high
  f = Image.new(2048, 1536) { self.background_color = "white" }
  f.write("splashscreen/ios/Default-Landscape@2x.png")

  #inform user
  puts "Splash screens created and dropped into /splashscreen/ios. These are blank splash screens for you to edit in your favorite image editor"
end

def androidsplash()
  FileUtils.mkdir_p 'splashscreen/android'
  FileUtils.mkdir_p 'splashscreen/android/xhdpi'
  FileUtils.mkdir_p 'splashscreen/android/hdpi'
  FileUtils.mkdir_p 'splashscreen/android/mdpi'
  FileUtils.mkdir_p 'splashscreen/android/ldpi'
  #Android splash xhdpi screen
    f = Image.new(720, 960) { self.background_color = "white" }
    f.write("splashscreen/android/xhdpi/screen.png")
  #Android splash hdpi screen
    f = Image.new(480, 640) { self.background_color = "white" }
    f.write("splashscreen/android/hdpi/screen.png")
  #Android splash mdpi screen
    f = Image.new(320, 470) { self.background_color = "white" }
    f.write("splashscreen/android/mdpi/screen.png")
  #Android splash ldpi screen
    f = Image.new(320, 426) { self.background_color = "white" }
    f.write("splashscreen/android/ldpi/screen.png")
end

def windows()
  FileUtils.mkdir_p 'icon-'+$dirname+'/windows-phone'
  makeicons('173x173', 'icon-173.png', 'windows-phone')
  makeicons('72x72', 'icon-72.png', 'windows-phone')
  makeicons('48x48', 'icon-48.png', 'windows-phone')
end

def webos()
  FileUtils.mkdir_p 'icon-'+$dirname+'/webos'
  makeicons('64x64', 'icon-64.png', 'webos')
end

case type
  when "A","a"
    android()
  when "B","b"
    blackberry()
  when "I","i"
    ios()
  when "W","w"
    windows()
  when "O","o"
    webos()
  when "E","e"
    android()
    blackberry()
    ios()
    windows()
    webos()
    iossplash()
    androidsplash()
  when "S","s"
    iossplash()
    androidsplash()
  when "T","t"
    testing()
  else
    puts "You input #{type} -- I have no idea what to do with that."
end
